package com.sylvain.weatherforecast.ui

import com.nhaarman.mockito_kotlin.given
import com.sylvain.weatherforecast.application.AppSchedulers
import com.sylvain.weatherforecast.application.initForTests
import com.sylvain.weatherforecast.domain.forecast.Forecast
import com.sylvain.weatherforecast.domain.forecast.ForecastService
import com.sylvain.weatherforecast.ui.forecast.list.ForecastListViewModel
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.threeten.bp.LocalDateTime
import org.threeten.bp.Month

@RunWith(MockitoJUnitRunner::class)
class ForecastListViewModelTest {

    @Mock
    private lateinit var service: ForecastService

    private lateinit var viewModel: ForecastListViewModel

    @Before
    fun before() {
        viewModel = ForecastListViewModel(forecastService = service)
        AppSchedulers.initForTests()
    }

    @Test
    fun `should return 2 DailyForecast`() {
        // given
        given(service.getForecast()).willReturn(Single.just(listOf(forecast, forecast2, forecast3)))

        // when
        val testObserver = viewModel.getForecast().test()

        // then
        testObserver.assertComplete()
            .assertValueCount(1)
            .assertValue { it.size == 2 }
    }

}

private val forecast = Forecast(
    temp = 10.0,
    tempMax = 12.0,
    tempMin = 10.0,
    date = LocalDateTime.of(2019, Month.APRIL, 10, 10, 0)
)

private val forecast2 = forecast.copy(date = LocalDateTime.of(2019, Month.APRIL, 11, 10, 0))

private val forecast3 = forecast.copy(date = LocalDateTime.of(2019, Month.APRIL, 11, 13, 0))