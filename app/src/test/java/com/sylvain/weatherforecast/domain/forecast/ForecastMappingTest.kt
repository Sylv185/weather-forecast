package com.sylvain.weatherforecast.domain.forecast

import com.google.common.truth.Truth
import com.sylvain.weatherforecast.data.forecast.api.ForecastContainerJson
import com.sylvain.weatherforecast.data.forecast.api.ForecastJson
import org.junit.Test
import org.threeten.bp.format.DateTimeFormatter

class ForecastMappingTest {

    @Test
    fun `should convert ForecastContainerJson to Forecast`() {
        // given
        val json = ForecastContainerJson(forecast = ForecastJson(temp = 21.07, tempMin = 18.12, tempMax = 21.07), date = "2019-08-19 09:00:00")

        // when
        val forecast = json.toForecast()

        // then
        Truth.assertThat(forecast.date.format(DateTimeFormatter.ISO_DATE_TIME)).isEqualTo("2019-08-19T09:00:00")
    }
}