package com.sylvain.weatherforecast.data.forecast.api

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ForecastApi {

    @GET("data/2.5/forecast")
    fun weatherForecast(@Query("id") id: Int? = PARIS_ID,
                        @Query("units") units: String? = UNITS_METRIC_SYSTEM,
                        @Query("appid") appId: String? = "5141ee25a33fe5ca319e9a5305455cd7"): Single<ForecastResponseJson>

}

private const val PARIS_ID = 6455259
private const val UNITS_METRIC_SYSTEM = "metric"
//"http://api.openweathermap.org/data/2.5/forecast?id=6455259&units=metric&appid=5141ee25a33fe5ca319e9a5305455cd7"