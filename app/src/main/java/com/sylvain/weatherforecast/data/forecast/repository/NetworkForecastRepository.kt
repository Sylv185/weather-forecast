package com.sylvain.weatherforecast.data.forecast.repository

import com.sylvain.weatherforecast.data.forecast.api.ForecastApi
import com.sylvain.weatherforecast.domain.forecast.Forecast
import com.sylvain.weatherforecast.domain.forecast.ForecastRepository
import com.sylvain.weatherforecast.domain.forecast.toForecastList
import io.reactivex.Single

class NetworkForecastRepository(private val forecastApi: ForecastApi) : ForecastRepository{

    override fun getForecast(): Single<List<Forecast>> {
        return forecastApi.weatherForecast().map { it.toForecastList() }
    }
}