package com.sylvain.weatherforecast.data.forecast.api

import com.google.gson.annotations.SerializedName

data class ForecastResponseJson(@SerializedName("list") val list: List<ForecastContainerJson>)

data class ForecastContainerJson(
    @SerializedName("main") val forecast: ForecastJson,
    @SerializedName("dt_txt") val date: String
)

data class ForecastJson(
    @SerializedName("temp") val temp: Double,
    @SerializedName("temp_min") val tempMin: Double,
    @SerializedName("temp_max") val tempMax: Double)