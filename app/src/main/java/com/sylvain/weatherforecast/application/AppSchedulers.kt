package com.sylvain.weatherforecast.application

import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import io.reactivex.android.schedulers.AndroidSchedulers as AndroidSchedulers

object AppSchedulers {

    private lateinit var io: Scheduler
    private lateinit var mainThread: Scheduler

    fun init(io: Scheduler = Schedulers.io(),
             mainThread: Scheduler = AndroidSchedulers.mainThread()) {
        AppSchedulers.io = io
        AppSchedulers.mainThread = mainThread
    }

    fun io() = io
    fun mainThread() = mainThread
}

fun AppSchedulers.initForTests() {
    init(
            io = Schedulers.trampoline(),
            mainThread = Schedulers.trampoline()
    )
}