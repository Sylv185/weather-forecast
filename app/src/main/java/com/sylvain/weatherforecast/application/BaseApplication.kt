package com.sylvain.weatherforecast.application

import android.app.Application
import android.os.Build
import com.jakewharton.threetenabp.AndroidThreeTen
import com.sylvain.weatherforecast.BuildConfig
import com.sylvain.weatherforecast.di.appModule
import com.sylvain.weatherforecast.di.dataModule
import com.sylvain.weatherforecast.di.domainModule
import com.sylvain.weatherforecast.di.networkModule
import org.koin.android.ext.android.startKoin
import timber.log.Timber

class BaseApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) Timber.plant(Timber.DebugTree())
        AppSchedulers.init()
        AndroidThreeTen.init(this)
        startKoin(this, listOf(networkModule, dataModule, domainModule, appModule))
    }
}
