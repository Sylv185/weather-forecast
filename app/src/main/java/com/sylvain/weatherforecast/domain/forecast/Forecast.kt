package com.sylvain.weatherforecast.domain.forecast

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime

@Parcelize
data class DailyForecast(
    val tempMin: Double,
    val tempMax: Double,
    val date: LocalDate,
    val forecastList: List<Forecast>
): Parcelable

@Parcelize
data class Forecast(
    val temp: Double,
    val tempMin: Double,
    val tempMax: Double,
    val date: LocalDateTime
): Parcelable