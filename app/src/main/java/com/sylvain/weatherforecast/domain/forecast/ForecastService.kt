package com.sylvain.weatherforecast.domain.forecast

class ForecastService(private val forecastRepository: ForecastRepository) {

    fun getForecast() = forecastRepository.getForecast()

}
