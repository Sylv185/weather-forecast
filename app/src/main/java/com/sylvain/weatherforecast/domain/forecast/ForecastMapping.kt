package com.sylvain.weatherforecast.domain.forecast

import com.sylvain.weatherforecast.data.forecast.api.ForecastContainerJson
import com.sylvain.weatherforecast.data.forecast.api.ForecastResponseJson
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter

fun ForecastResponseJson.toForecastList() = list.map { it.toForecast() }

fun ForecastContainerJson.toForecast() = with(this.forecast) {
    Forecast(
        temp = temp,
        tempMin = tempMin,
        tempMax = tempMax,
        date = LocalDateTime.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
    )
}

fun List<Forecast>.toDailyForecast() = DailyForecast(
    tempMin = this.minBy { it.tempMin }!!.tempMin, // !! is not good, but 4hours only
    tempMax = this.maxBy { it.tempMax }!!.tempMax,
    date = this.first().date.toLocalDate(),
    forecastList = this)

