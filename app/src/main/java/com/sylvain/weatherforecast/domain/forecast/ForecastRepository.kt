package com.sylvain.weatherforecast.domain.forecast

import io.reactivex.Single

interface ForecastRepository {
    fun getForecast(): Single<List<Forecast>>
}