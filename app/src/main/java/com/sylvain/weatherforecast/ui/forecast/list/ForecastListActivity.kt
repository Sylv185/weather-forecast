package com.sylvain.weatherforecast.ui.forecast.list

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.sylvain.weatherforecast.R
import com.sylvain.weatherforecast.application.AppSchedulers
import com.sylvain.weatherforecast.domain.forecast.DailyForecast
import com.sylvain.weatherforecast.ui.forecast.detail.DailyForecastActivity
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import kotlinx.android.synthetic.main.forecast_list_activity.*
import org.koin.android.ext.android.inject
import timber.log.Timber

class ForecastListActivity : AppCompatActivity() {


    private val disposable = CompositeDisposable()

    private val viewModel: ForecastListViewModel by inject()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.forecast_list_activity)

        initWidgets()
    }

    override fun onResume() {
        super.onResume()
        disposable += viewModel.getForecast()
            .subscribeOn(AppSchedulers.io())
            .observeOn(AppSchedulers.mainThread())
            .subscribe(
                { dailyForecastList ->
                    (forecast_list_thumbnails.adapter as DailyForecastAdapter).data = dailyForecastList
                },
                { t ->
                    Toast.makeText(this, "Une erreur est survenue", Toast.LENGTH_LONG).show()
                    Timber.e(t)
                }
            )
    }

    override fun onPause() {
        disposable.clear()
        super.onPause()
    }

    private fun initWidgets() {
        forecast_list_thumbnails.adapter =
            DailyForecastAdapter(listener = object :
                DailyForecastAdapter.OnItemClickListener {
                override fun onItemClicked(forecast: DailyForecast) {
                    startActivity(
                        DailyForecastActivity.createIntent(
                            this@ForecastListActivity,
                            forecast
                        )
                    )
                }
            })
        forecast_list_thumbnails.layoutManager = LinearLayoutManager(this)
    }
}
