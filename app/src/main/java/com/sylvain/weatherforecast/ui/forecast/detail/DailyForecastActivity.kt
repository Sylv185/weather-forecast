package com.sylvain.weatherforecast.ui.forecast.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.sylvain.weatherforecast.R
import com.sylvain.weatherforecast.domain.forecast.DailyForecast
import kotlinx.android.synthetic.main.daily_forecast_activity.*

class DailyForecastActivity : AppCompatActivity() {

    companion object {
        private const val FORECAST_EXTRA = "forecast"
        fun createIntent(source: Context, forecast: DailyForecast) =
            Intent(source, DailyForecastActivity::class.java).apply {
                this.putExtra(FORECAST_EXTRA, forecast)
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.daily_forecast_activity)

        daily_forecast_list.layoutManager = LinearLayoutManager(this)
        daily_forecast_list.adapter = ForecastAdapter()

        intent?.extras?.getParcelable<DailyForecast>(FORECAST_EXTRA)?.let {
            (daily_forecast_list.adapter as ForecastAdapter).data = it.forecastList
        }
    }
}
