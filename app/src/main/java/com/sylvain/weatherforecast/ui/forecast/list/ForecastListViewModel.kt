package com.sylvain.weatherforecast.ui.forecast.list

import com.sylvain.weatherforecast.domain.forecast.ForecastService
import com.sylvain.weatherforecast.domain.forecast.toDailyForecast

class ForecastListViewModel(private val forecastService: ForecastService) {

    fun getForecast() = forecastService.getForecast().map { forecastList ->
        forecastList.groupBy { forecast -> forecast.date.dayOfYear }
            .map { forecastMap -> forecastMap.value.toDailyForecast() }
    }
}
