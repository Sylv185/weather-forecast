package com.sylvain.weatherforecast.ui.forecast.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.sylvain.weatherforecast.R
import com.sylvain.weatherforecast.domain.forecast.DailyForecast
import kotlinx.android.extensions.LayoutContainer
import org.threeten.bp.format.DateTimeFormatter

class DailyForecastAdapter(val listener: OnItemClickListener) :
    RecyclerView.Adapter<DailyForecastAdapter.DailyForecastViewHolder>() {

    interface OnItemClickListener {
        fun onItemClicked(forecast: DailyForecast)
    }

    var data: List<DailyForecast> = emptyList()
        set(newList) {
            field = newList
            notifyDataSetChanged()
        }

    override fun getItemCount(): Int = data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DailyForecastViewHolder {
        return DailyForecastViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.daily_forecast_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: DailyForecastViewHolder, position: Int) {
        holder.bind(data[position])
    }

    inner class DailyForecastViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
        LayoutContainer {
        fun bind(item: DailyForecast) = with(itemView) {
            this.findViewById<TextView>(R.id.forecast_item_time).text =
                item.date.format(DateTimeFormatter.ofPattern("EEE"))
            this.findViewById<TextView>(R.id.forecast_temp_min).text =
                containerView.context.getString(R.string.temperature_celsius, item.tempMin)
            this.findViewById<TextView>(R.id.forecast_item_temp_max).text =
                containerView.context.getString(R.string.temperature_celsius, item.tempMax)
            this.setOnClickListener { v ->
                listener.onItemClicked(item)
            }
        }
    }
}
