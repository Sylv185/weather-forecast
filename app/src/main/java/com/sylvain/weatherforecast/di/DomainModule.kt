package com.sylvain.weatherforecast.di

import com.sylvain.weatherforecast.domain.forecast.ForecastService
import org.koin.dsl.module.module

val domainModule = module {
    single { ForecastService(forecastRepository = get()) }
}