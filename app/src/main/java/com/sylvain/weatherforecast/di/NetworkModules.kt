package com.sylvain.weatherforecast.di

import com.sylvain.weatherforecast.BuildConfig
import com.sylvain.weatherforecast.data.forecast.api.ForecastApi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {

    single<GsonConverterFactory> { GsonConverterFactory.create() }
    single<OkHttpClient> { buildOkHttpClient() }
    single { buildRetrofit(BuildConfig.BASE_URL, get(), get()).create(ForecastApi::class.java) }
}


private fun buildRetrofit(baseUrl: String, okHttpClient: OkHttpClient, gsonConverterFactory: GsonConverterFactory) =
    Retrofit.Builder()
        .baseUrl(baseUrl)
        .client(okHttpClient)
        .addConverterFactory(gsonConverterFactory)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()


private fun buildOkHttpClient() =
    OkHttpClient().newBuilder()
        .addInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY })
        .build()
