package com.sylvain.weatherforecast.di

import com.sylvain.weatherforecast.data.forecast.repository.NetworkForecastRepository
import com.sylvain.weatherforecast.domain.forecast.ForecastRepository
import org.koin.dsl.module.module

val dataModule = module {
    single<ForecastRepository> { NetworkForecastRepository(forecastApi = get()) }

}