package com.sylvain.weatherforecast.di

import com.sylvain.weatherforecast.ui.forecast.list.ForecastListViewModel
import org.koin.dsl.module.module

val appModule = module {
    single { ForecastListViewModel(forecastService = get()) }
}
