#### Architecture
- Simple layer architecture UI / Domain / Data
- MVVM architecture UI side to simplify code reading and tests

#### Libs
- Koin for dependency injection (easy to set up and works fine)
- RxJava for asynchronous
- Retrofit for WS call (event if it's not used, it's ready to test when server will be up again)
- Mockito for tests
- Timber for logging


#### Improvement with more time
- Do a better UI/UX design
- Unit testing
- Handle exception / ws errors
- So much things to do... :)